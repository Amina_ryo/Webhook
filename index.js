const express = require("express");
const bodyParser = require("body-parser");

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const port = 8000;
app.listen(port, () => {
  console.log(`server is running on port ${port}`);
});

app.get('/api', (req,res) => {
    res.send('hi');
})

app.post('/api/github-webhook', (req,res) => {
  // let payload;
  // if(req.body.action){
  //   payload = {
  //     pushedBranch : req.body.ref,
  //     repo : req.body.repository.name,
  //     pusher : req.body.pusher.name,
  //     sender: req.body.sender.login,
  //     commits : req.body.commits
  //   }
  // }
  // else if(req.body.action == 'opened'){
  //   payload = {
  //     action: req.body.action,
  //     number : req.body.number,
  //     message: req.body.pull_request.title,
  //     time: req.body.pull_request.created_at,
  //     from: req.body.pull_request.head.ref,
  //     to: req.body.pull_request.base.ref,
  //     repo : req.body.repository.name,
  //     name: req.body.sender.login
  //   }
  // }
  // else if(req.body.action == 'closed'){
  //   payload = {
  //     action: req.body.action,
  //     number : req.body.number, 
  //     time: req.body.pull_request.merged_at,
  //     from: req.body.pull_request.head.ref,
  //     to: req.body.pull_request.base.ref,
  //     name: req.body.pull_request.merged_by.login,
  //     repo : req.body.repository.name,
  //     sender: req.body.sender.login,
  //   }
  // }
  console.log(req.body);
})


